import java.util.Random;
public class Die {
	private int pips;
	private Random ranDie;
	
	public Die() {
		this.pips = 1;
		this.ranDie = new Random();
	}
	
	public int getPips() {
		return this.pips;
	}
	public Random getRanDie() {
		return this.ranDie;
	}
	
	public int roll() {
		int randomRoll = this.ranDie.nextInt(1, 6 + 1);
		this.pips = randomRoll;
		return this.pips;
	}
	
	public String toString() {
		String theDie = "The die displays a " + getPips();
		return theDie;
	}
}