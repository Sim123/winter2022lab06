public class ShutTheBox {
	public static void main(String[] args) {
		System.out.println("Welcome players!");
		Board newGame = new Board();
		boolean gameOver = false;
		
		for (int i = 1; i < newGame.closedTiles.length;i++) {
			while (gameOver != true) {
				boolean result;
				System.out.println("Player 1’s turn!");
				System.out.println(newGame.toString());
				result = newGame.playATurn();
				if (result == true) {
					System.out.println("Player 2 wins!");
					gameOver = true;
				} else {
					System.out.println("Player 2’s turn!");
					System.out.println(newGame.toString());
					result = newGame.playATurn();
					if (result == true) {
						System.out.println("Player 1 wins!");
						gameOver = true;
					}
				}
			}
		 }
	 }
 }