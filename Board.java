public class Board {
	private Die die1;
	private Die die2;
	public boolean [] closedTiles;
	
	public Board() {
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new boolean[12];
	}
	
	public String toString() {
			String all = "";
			int boxes = 1;
			for (int i = 0; i < closedTiles.length; i++) {
				if (closedTiles[i]== true) {
					all += " x";
				} else if (closedTiles[i]== false) {
					all = all + " " + boxes;
			}
			boxes++;
		}
		return all; 
	}
	
	public boolean playATurn() {
		int roll1 = this.die1.roll();
		System.out.println(this.die1.toString());
		int roll2 = this.die2.roll();
		System.out.println(this.die2.toString());
		int sum = roll1 + roll2;
		
			if (closedTiles[sum-1]== true) {
				closedTiles[sum-1] = true;
				System.out.println("The tile at this position is already shut!");
				return true;
			} else if (closedTiles[sum-1]== false) {
				closedTiles[sum-1] = true;
					System.out.println("Closing tile: " + sum);
					return false;
			}
		return false;
	}
}
